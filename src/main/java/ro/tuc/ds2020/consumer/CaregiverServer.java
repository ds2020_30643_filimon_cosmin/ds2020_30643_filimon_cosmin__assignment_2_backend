package ro.tuc.ds2020.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import ro.tuc.ds2020.entities.PatientActivity;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashSet;
import java.util.Set;

public class CaregiverServer extends WebSocketServer {

    private final static Logger logger = LogManager.getLogger(CaregiverServer.class);

    private static Set<WebSocket> conns;

    private CaregiverServer(int port) {
        super(new InetSocketAddress(port));
        conns = new HashSet<>();
    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        conns.add(webSocket);

        logger.info("Connection established from: " + webSocket.getRemoteSocketAddress().getHostString());
        System.out.println("New connection from " + webSocket.getRemoteSocketAddress().getAddress().getHostAddress());
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        conns.remove(conn);
        // When connection is closed, remove the user.
        try {
            removeUser(conn);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        logger.info("Connection closed to: " + conn.getRemoteSocketAddress().getHostString());
        System.out.println("Closed connection to " + conn.getRemoteSocketAddress().getAddress().getHostAddress());
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Message msg = mapper.readValue(message, Message.class);

            switch (msg.getType()) {
                case USER_JOINED:
                    addCaregiver(msg.getCaregiverId(), conn);
                    break;
                case USER_LEFT:
                    removeUser(conn);
                    break;
                case TEXT_MESSAGE:
                    broadcastMessage(msg);
                    break;
                default:
                    break;
            }

            System.out.println("Message from user: " + msg.getCaregiverId() + ", text: " + msg.getPatientActivity().toString() + ", type:" + msg.getType());
            logger.info("Message from user: " + msg.getCaregiverId() + ", text: " + msg.getPatientActivity().toString());
        } catch (IOException e) {
            logger.error("Wrong message format.");
            // return error message to user
        }
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {

        if (conn != null) {
            conns.remove(conn);
        }
        assert conn != null;
        System.out.println("ERROR from " + conn.getRemoteSocketAddress().getAddress().getHostAddress());
    }

    @Override
    public void onStart() {

    }

    public void broadcastMessage(Message msg) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String messageJson = mapper.writeValueAsString(msg);
            for (WebSocket sock : conns) {
                sock.send(messageJson);
            }
        } catch (JsonProcessingException e) {
            logger.error("Cannot convert message to json.");
        }
    }

    private void addCaregiver(Integer caregiverId, WebSocket conn) throws JsonProcessingException {
        acknowledgeUserJoined(caregiverId, conn);
        broadcastUserActivityMessage(MessageType.USER_JOINED);
    }

    private void removeUser(WebSocket conn) throws JsonProcessingException {
        broadcastUserActivityMessage(MessageType.USER_LEFT);
    }

    private void acknowledgeUserJoined(Integer caregiverId, WebSocket conn) throws JsonProcessingException {
        Message message = new Message();
        message.setType(MessageType.USER_JOINED_ACK);
        message.setCaregiverId(caregiverId);
        conn.send(new ObjectMapper().writeValueAsString(message));
    }

    private void broadcastUserActivityMessage(MessageType messageType) throws JsonProcessingException {

        Message newMessage = new Message();

        ObjectMapper mapper = new ObjectMapper();
        newMessage.setPatientActivity(new PatientActivity(77, "XXXXXX", 12345, 12345));
        newMessage.setType(messageType);
        broadcastMessage(newMessage);
    }

    public static void startServer() {
        int port;
        try {
            port = Integer.parseInt(System.getenv("PORT"));
        } catch (NumberFormatException nfe) {
            port = 9000;
        }
        new CaregiverServer(port).start();

        FileConsumer fileConsumer = new FileConsumer(conns);
        try {
            fileConsumer.consume();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startConsumer() {

    }
}
