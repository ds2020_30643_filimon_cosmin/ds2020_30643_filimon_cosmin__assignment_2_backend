package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.entities.Medication;

public class MedicationBuilder {
    public MedicationBuilder() {
    }

    public static MedicationDTO generateDTOFromEntity(Medication medication) {
        if(medication == null) return null;
        return new MedicationDTO(
                medication.getId(),
                medication.getName(),
                medication.getSideEffects(),
                medication.getDosage()
        );
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO) {
        if(medicationDTO == null) return null;
        return new Medication(
                medicationDTO.getName(),
                medicationDTO.getSideEffects(),
                medicationDTO.getDosage()
        );
    }
}
