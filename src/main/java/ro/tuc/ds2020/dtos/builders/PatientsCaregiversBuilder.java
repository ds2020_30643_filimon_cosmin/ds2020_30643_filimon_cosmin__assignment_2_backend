package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PatientsCaregiversDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;

public class PatientsCaregiversBuilder {
    public PatientsCaregiversBuilder() {
    }

    public static PatientsCaregiversDTO generateDTOFromEntity(Patient patient) {
        if(patient == null) return null;
        return new PatientsCaregiversDTO(
                patient.getId(),
                patient.getName(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getAddress(),
                CaregiverBuilder.generateDTOFromEntity(patient.getCaregiver())
        );
    }

    public static Patient generateEntityFromDTO(PatientsCaregiversDTO patientsCaregiversDTO) {
        if(patientsCaregiversDTO == null) return null;
        Caregiver caregiver = CaregiverBuilder.generateEntityFromDTO(patientsCaregiversDTO.getCaregiver());
        caregiver.setId(patientsCaregiversDTO.getCaregiver().getId());
        return new Patient(
                patientsCaregiversDTO.getName(),
                patientsCaregiversDTO.getBirthdate(),
                patientsCaregiversDTO.getGender(),
                patientsCaregiversDTO.getAddress(),
                caregiver
        );
    }
}
