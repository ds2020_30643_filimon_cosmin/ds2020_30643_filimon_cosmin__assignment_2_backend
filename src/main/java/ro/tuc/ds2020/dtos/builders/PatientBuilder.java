package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Patient;

public class PatientBuilder {
    public PatientBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient patient) {
        if(patient == null) return null;
        return new PatientDTO(
                patient.getId(),
                patient.getName(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getAddress()
        );
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO) {
        if(patientDTO == null) return null;
        return new Patient(
                patientDTO.getName(),
                patientDTO.getBirthdate(),
                patientDTO.getGender(),
                patientDTO.getAddress()
        );
    }
}
