package ro.tuc.ds2020.dtos;

public class MedicationPlanDTO {
    private Integer id;
    private String intakeIntervals;
    private String startDate;
    private String endDate;

    public MedicationPlanDTO() {
    }

    public MedicationPlanDTO(Integer id, String intakeIntervals, String startDate, String endDate) {
        this.id = id;
        this.intakeIntervals = intakeIntervals;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
