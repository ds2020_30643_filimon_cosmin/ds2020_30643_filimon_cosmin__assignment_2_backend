package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiversPatientsDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.CaregiversPatientsBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.repositories.CaregiverRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public List<CaregiverDTO> findAll() {
        List<Caregiver> caregivers = caregiverRepository.findAll();

        return caregivers.stream()
                .map(CaregiverBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<CaregiversPatientsDTO> findAllWithPatients() {
        List<Caregiver> caregivers = caregiverRepository.findAll();

        return caregivers.stream()
                .map(CaregiversPatientsBuilder::generateDTOFomEntity)
                .collect(Collectors.toList());
    }


    public Integer insert(CaregiverDTO caregiverDTO) {


        return caregiverRepository
                .save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO))
                .getId();
    }


    public CaregiversPatientsDTO findCaregiverById(Integer id) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);



        return CaregiversPatientsBuilder.generateDTOFomEntity(caregiver.get());
    }

    public Integer update(CaregiverDTO caregiverDTO) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getId());


        Caregiver updatedCaregiver = CaregiverBuilder.generateEntityFromDTO(caregiverDTO);
        updatedCaregiver.setId(caregiverDTO.getId());

        return caregiverRepository
                .save(updatedCaregiver)
                .getId();
    }


    public void delete(CaregiverDTO caregiverDTO) {
        caregiverRepository.deleteById(caregiverDTO.getId());
    }
}
