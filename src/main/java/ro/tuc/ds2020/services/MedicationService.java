package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }


    public List<MedicationDTO> findAll() {
        List<Medication> medications = medicationRepository.findAll();

        return medications.stream()
                .map(MedicationBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public Integer insert(MedicationDTO medicationDTO) {

        return medicationRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getId();
    }


    public MedicationDTO findMedicationById(Integer id) {
        Optional<Medication> medication = medicationRepository.findById(id);


        return MedicationBuilder.generateDTOFromEntity(medication.get());
    }

    public Integer update(MedicationDTO medicationDTO) {
        Optional<Medication> medication = medicationRepository.findById(medicationDTO.getId());


        Medication updatedMedication = MedicationBuilder.generateEntityFromDTO(medicationDTO);
        updatedMedication.setId(medicationDTO.getId());

        return medicationRepository
                .save(updatedMedication)
                .getId();
    }

    public void delete(MedicationDTO medicationDTO) {
        medicationRepository.deleteById(medicationDTO.getId());
    }
}
