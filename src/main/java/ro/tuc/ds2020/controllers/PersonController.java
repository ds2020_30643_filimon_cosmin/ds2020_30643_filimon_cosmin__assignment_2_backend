package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.services.PersonService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping()
    public ResponseEntity<List<PersonDTO>> getPersons() {
        List<PersonDTO> dtos = personService.findPersons();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody PersonDetailsDTO personDTO) {
        UUID personID = personService.insert(personDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PersonDTO> getPerson(@PathVariable("id") UUID personId) {
        PersonDTO dto = personService.findPersonById(personId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //TODO: UPDATE, DELETE per resource
   /* @PutMapping(value = "/{id}")
    public ResponseEntity<PersonDTO> deletePerson(@PathVariable("id") UUID personId) {
        PersonDTO dto = personService.delete(personId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }*/
    @PutMapping(value = "/{id}")
    public ResponseEntity<PersonDTO> updateProsumer(@Valid @RequestBody PersonDetailsDTO personDTO, @PathVariable("id") UUID personID ) {
        PersonDTO person= personService.findPersonById(personID);
        UUID updatedPerson = personService.update(personDTO);
        return new ResponseEntity<>( HttpStatus.CREATED);

    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") UUID personId){
        personService.delete(personId);
    }
}
