package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.PatientActivity;
import ro.tuc.ds2020.services.PatientActivityService;

@RestController
@CrossOrigin
@RequestMapping(value = "/patientactivity")
public class PatientActivityController {
    private final PatientActivityService patientActivityService;

    @Autowired
    public PatientActivityController(PatientActivityService patientActivityService) {
        this.patientActivityService = patientActivityService;
    }

    // Create medication
    @PostMapping()
    public Integer insertMedication(@RequestBody PatientActivity patientActivity) {
        return patientActivityService.insert(patientActivity);
    }
}
