package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiversPatientsDTO;
import ro.tuc.ds2020.services.CaregiverService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;
    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }


    @GetMapping()
    public List<CaregiverDTO> findAll() {
        return caregiverService.findAll();
    }


    @GetMapping(value = "/patients")
    public List<CaregiversPatientsDTO> findAllWithPatients() {
        return caregiverService.findAllWithPatients();
    }


    @PostMapping
    public Integer insertCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.insert(caregiverDTO);
    }

    @GetMapping(value = "/{id}")
    public CaregiversPatientsDTO findById(@PathVariable("id") Integer id) {
        return caregiverService.findCaregiverById(id);
    }

    @PutMapping()
    public Integer updateCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.update(caregiverDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody CaregiverDTO caregiverDTO) {
        caregiverService.delete(caregiverDTO);
    }
}
