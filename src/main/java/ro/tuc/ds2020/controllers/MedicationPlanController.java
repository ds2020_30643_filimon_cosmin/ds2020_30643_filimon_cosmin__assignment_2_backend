package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedPlansPatientsMedicationsDTO;
import ro.tuc.ds2020.services.MedicationPlanService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationplan")
public class MedicationPlanController {
    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @GetMapping()
    public List<MedPlansPatientsMedicationsDTO> findAll() {
        return medicationPlanService.findAll();
    }

    @PostMapping()
    public Integer insertMedicationPlan(@RequestBody MedPlansPatientsMedicationsDTO medPlansPatientsMedicationsDTO) {
        return medicationPlanService.insert(medPlansPatientsMedicationsDTO);
    }
}
